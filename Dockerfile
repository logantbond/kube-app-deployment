FROM python:3.8 as builder

# do requirements first since dependencies should change less frequently
COPY requirements.txt .
RUN pip install --user -r requirements.txt

# shrinks dependency size with multistage
FROM python:3.8-slim

WORKDIR /app

COPY --from=builder /root/.local /home/appuser/.local
COPY app.py /app

# apt update/upgrade to help mitigate: https://security-tracker.debian.org/tracker/CVE-2020-29363
RUN apt update && apt -y upgrade  && \
    apt install -y curl           && \
    useradd --create-home appuser && \
    chown -R appuser:appuser /home/appuser

USER appuser

ENV PATH=/home/appuser/.local/bin:$PATH
EXPOSE 5000

HEALTHCHECK CMD curl --fail http://localhost:5000/health || exit 1   

CMD ["python3", "app.py"]