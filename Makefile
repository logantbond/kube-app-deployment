run:
	python3 app.py

deps:
	pip3 install --user -r requirements.txt pytest pytest-cov pylint flake8

lint:
	flake8 --max-line-length=120 *.py
	pylint *.py

.PHONY: test
test:
	pytest --cov=app .

.PHONY: deploy
deploy:
	helm install ./deploy --generate-name

docker:
	docker build -t flask-hello-world .
	docker run -d --name flask-hello-world --restart unless-stopped -p 5000:5000 flask-hello-world

clean:
	docker stop flask-hello-world || exit 0
	docker rm flask-hello-world || exit 0

all: deps lint test docker