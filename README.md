# Simple Kube App Deployment

The goal of this coding exercise is to take sample code, clean it up, and deploy it to kubernetes.

**A note about the approach to the initial code provided:**

I saw that it was a simple hello world application with some added strangeness, and I wasn't sure what was allowed vs. not in terms of editing the code. I found it easiest to just remove the extraneous lines and flip it back to "Hello, world!" to focus more on the pipeline/container/deployment. Therefore, I removed the static folder (which wasn't used in the original code), removed the comments disabling lint (also fixed lint), commented out the random number generator logic for the root endpoint, and added some simple unit tests. To me, "include whatever would be needed to make it production-ready" means setting up the project with the correct foundation to keep development moving forward, make it safer to fail, and build metrics/reporting about the health of the application/repository.

#### Tech Stack:
* Python Flask application (`"Hello, world!"`)
* Dockerfile: [link to registry](https://gitlab.com/logantbond/kube-app-deployment/container_registry)
* Helm chart for K8s deployment
* Gitlab CICD: [link to pipeline history](https://gitlab.com/logantbond/kube-app-deployment/-/pipelines)

## Running Locally:

**Once up and running, visit http://localhost:5000 to view the app (as described below)!**

#### Cloning from Source

Getting up and running from source is as easy as cloning the repo, `cd`ing to the directory, and running `python3 app.py`

```bash
╰─ git clone git@gitlab.com:logantbond/kube-app-deployment.git
Cloning into 'kube-app-deployment'...
remote: Enumerating objects: 34, done.
remote: Total 34 (delta 0), reused 0 (delta 0), pack-reused 34
Receiving objects: 100% (34/34), 17.07 KiB | 8.53 MiB/s, done.
Resolving deltas: 100% (11/11), done.

╰─ cd kube-app-deployment

╰─ python3 app.py
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```

#### Using the `Makefile`

The Makefile is the easiest way to get up and running with the application after cloning from source. It may be easiest to just open the Makefile and read, but here is a brief explanation of each target:

| Target | Definition |
| ------ | ---------- |
| `make run` | simply runs the python app (`python3 app.py`) |
| `make deps` | uses `pip3` to install `requirements.txt`, linting tools, and `pytest` |
| `make lint` | runs `flake8` and `pylint` |
| `make test` | runs tests with `pytest` showing code coverage |
| `make deploy` | deploys the application with helm (assumes the user has helm, kubectl, and a valid kubeconfig setup) |
| `make docker` | builds and runs the docker container |
| `make clean` | stops the docker container and removes it |
| `make all` | `deps`, `lint`, `test`, and `docker` |

#### Running with Docker

If the repo is cloned locally, build the image first:

```bash
╰─ docker build -t flask-hello-world .
Sending build context to Docker daemon    386kB
Step 1/11 : FROM python:3.8 as builder
...
...
...
Successfully tagged flask-hello-world:latest
```

Then run the freshly built image:

```bash
╰─ docker run --name my-flask-hello-world-app -p 5000:5000 flask-hello-world:latest                           
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```

Or you can run the latest, stable image off of the master branch straight from the registry (will require login to registry.gitlab.com):

```
╰─ docker pull registry.gitlab.com/logantbond/kube-app-deployment/master
```

NOTE: The Dockerfile exposes port 5000 by default.

#### Running with Helm

The repository uses helm in 2 ways:

1. GitLab's AutoDevOps
2. A helm chart provided in `deploy/`

GitLab's AutoDevOps will be explained in a later section, but to get started locally with the provided helm chart, ensure you have installed `helm` + `kubectl` and have a kubernetes cluster with a valid kubeconfig.

The provided helm chart was generated using `helm create`, which builds an out-of-the-box, robust chart ready for production use. I modified it slightly to provide it the correct paths for readiness/liveness probes, update `NOTES.txt`, and configured a few defaults for things like service ports. 

To deploy, you can edit the provided `values.yaml` or use defaults:
```bash
╰─ helm install ./deploy --generate-name
NAME: deploy-1609731468
LAST DEPLOYED: Sun Jan  3 21:37:51 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=deploy,app.kubernetes.io/instance=deploy-1609731468" -o jsonpath="{.items[0].metadata.name}")
  echo "Visit http://localhost:5000 to use your application"
  kubectl --namespace default port-forward $POD_NAME 5000:5000
```

## Project Landscape/Setup

This project has a few more nuances than just the code stored within. They are documented below to give a fuller picture of this repository's implementation of DevOps.

1. This project has enforced fast-forward merging and encouraged commit squashing (though fast-forward merging was turned on later in the project lifecycle - I initially forgot to save the setting change :face_palm:) [link to project graph](https://gitlab.com/logantbond/kube-app-deployment/-/network/master)

2. Nobody can push directly to the master branch, not even maintainers

3. This project is configured for GitLab's AutoDevOps with a few extensions:

   - Added lint to the `test` stage
   - Overrode the AutoDevOps unit testing with a test job that gathers code coverage metrics
   - Enabled the kubesec add-on to the SAST scanning tools
   - Disabled the headless postgres service (not needed for hello world)
   - [Link to .gitlab-ci.yml](https://gitlab.com/logantbond/kube-app-deployment/-/blob/master/.gitlab-ci.yml)
   <br />

4. There is an [integrated kube cluster running in GKE](https://gitlab.com/logantbond/kube-app-deployment/-/clusters) (just created a free trial account)

   - My preferred method of kubernetes deployment is to integrate an application in GitLab directly with a kubernetes cluster. With this integration comes AutoDevOps' auto-packaging/creation of a helm chart. If AutoDevOps detects a helm chart in `chart/`, it will use that instead of the built in chart. I wanted to do both the integrated and generated one with `helm create` to show both paths (which is why the helm chart provided is in `deploy/`, not `chart/`)
   - The kube cluster has [Prometheus](https://gitlab.com/logantbond/kube-app-deployment/-/metrics?environment=3768149) and an [Elastic Stack](https://gitlab.com/logantbond/kube-app-deployment/-/logs) installed
   - [View the current, running environments](https://gitlab.com/logantbond/kube-app-deployment/-/environments)
   <br />

5. AutoDevOps is configured to deploy straight to production from the master branch. The current production environment can be [viewed here](http://logantbond-kube-app-deployment.35.202.172.132.nip.io/).