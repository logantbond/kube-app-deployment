'''
Unit tests for app.py
'''

import unittest
from app import app


class TestApp(unittest.TestCase):
    '''
    TestApp is a class containing methods for testing the routes in app.py
    '''

    # def __init__(self):
    #     super().__init__(self)
    #     self.test_app = app.test_client()

    def test_root(self):
        '''
        A test case for / in app.py
        '''

        test_app = app.test_client()

        print("Making test request to / ...")
        response = test_app.get('/')

        self.assertEqual(response.status, "200 OK")
        self.assertEqual(response.data, b"Hello, world!")

    def test_health(self):
        '''
        A test case for /health in app.py
        '''

        test_app = app.test_client()

        print("Making test request to /health ...")
        response = test_app.get('/health')

        self.assertEqual(response.status, "200 OK")
        self.assertEqual(response.data, b"I'm alive!")
